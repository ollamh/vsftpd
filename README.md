[![pipeline status](https://gitlab.com/docker-files1/vsftpd/badges/development/pipeline.svg)](https://gitlab.com/docker-files1/vsftpd/-/commits/development) ![Project version](https://img.shields.io/docker/v/oscarenzo/vsftpd?sort=date) ![Docker image pulls](https://img.shields.io/docker/pulls/oscarenzo/vsftpd) ![Docker image size](https://img.shields.io/docker/image-size/oscarenzo/vsftpd?sort=date) ![Project license](https://img.shields.io/gitlab/license/docker-files1/vsftpd)
# vsftpd
Dockerfile to create containers with vsftpd service.

## 🧾 Components
 - Debian
 - vsftpd

## 🔗 References
https://github.com/fauria/docker-vsftpd

## ✍️ Advices
For persistent volume configuration you may think on this paths:

```
/srv_volume/
```

### Compliance
 - Disable anonymous read/write
 - Disable recurse ls for broken FTP clients
 - Enable verbose and separate log
 - Using chroot connections
 - Support for SSL / TLS

### Customization
 - Set IDLE timeout to 600s
 - Disable IPv6
 - Enable passive mode

## ⚙️ Environment variables
This image uses environment variables to allow the configuration of some parameters at run time:

* Variable name: `FTP_USER`
* Default value: admin
* Accepted values: Any string. Avoid whitespaces and special chars.
* Description: Username for the default FTP account. If you don't specify it through the `FTP_USER` environment variable at run time, `admin` will be used by default.

----

* Variable name: `FTP_PASSWORD`
* Default value: Random string.
* Accepted values: Any string.
* Description: If you don't specify a password for the default FTP account through `FTP_PASSWORD`, a 16 character random string will be automatically generated. You can obtain this value through the [container logs](https://docs.docker.com/engine/reference/commandline/container_logs/).

----

* Variable name: `PASV_PROMISCUOUS`
* Default value: false
* Accepted values: Use `false` for disable, values accepted are: [ true | false ].
* Description: This will indicate if the ftp server should run the security check that ensure the data connection originates from the same ip address as the control connection, by default it will check.

----

* Variable name: `PASV_ADDRESS`
* Default value: Docker host IP / Hostname.
* Accepted values: Any IPv4 address or Hostname.
* Description: If you don't specify an IP address to be used in passive mode, the routed IP address of the Docker host will be used. Bear in mind that this could be a local address.

----

* Variable name: `PASV_MIN_PORT`
* Default value: 4559
* Accepted values: Any valid port number.
* Description: This will be used as the lower bound of the passive mode port range. Remember to publish your ports with `docker -p` parameter.

----

* Variable name: `PASV_MAX_PORT`
* Default value: 4564
* Accepted values: Any valid port number.
* Description: This will be used as the upper bound of the passive mode port range. It will take longer to start a container with a high number of published ports.

----

* Variable name: `FTP_REPOSITORY`
* Default value: /srv_volume
* Accepted values: Any valid server local path.
* Description: This will be used as user home directory.

----

* Variable name: `FTP_SERVER_NAME`
* Default value: Welcome to My FTP service
* Accepted values: Any string for refer to your ftp server.
* Description: This will be used as ftp server description in the `Welcome message`.

----

* Variable name: `USESSL`
* Default value: false
* Accepted values: Use `false` for disable, values accepted are: [ true | false ].
* Description: This will indicate if the ftp server will use SSL / TLS connections, in case that you use Self-signed certificate, by default is issued for 365 days.

----

* Variable name: `FORCESSL`
* Default value: false
* Accepted values: Use `true` for force, values accepted are: [ true | false ].
* Description: This will indicate if the ftp server will force to use SSL / TLS connections, by default the SSL / TLS is not forced.

----

* Variable name: `SSL_PRIVATE_KEY`
* Default value: empty
* Accepted values: STRING.
* Description: Map the private key file if you do not want to use self-signed certificate [ `/etc/ssl/private/vsftpd.key` ].

----

* Variable name: `SSL_CERTIFICATE`
* Default value: empty
* Accepted values: STRING.
* Description: Map the certificate file if you do not want to use self-signed certificate [ `/etc/ssl/certs/vsftpd.crt` ].

## 💬 Legend
* NKS => No key sensitive
* KS => Key sensitive
