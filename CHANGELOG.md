# [1.13.0](/Docker_Files/vsftpd/compare/1.13.0...master)
* Docker linter apply and little hotfix that might affect to some files that match with the current `.gitignore`

# [1.12.0](/Docker_Files/vsftpd/compare/1.12.0...master)
* Se actualiza el formato de la documentación

# [1.11.0](/Docker_Files/vsftpd/compare/1.11.0...master)
* Se configuran las versiones del `CHANGELOG.md` para que sean revisables por la herramienta de comparación de **Gitea**
* Se configura para que los logs de *vsftpd.log* y *xferlog.log* vayan al stdout
* Se ajusta la cadena de arranque del servicio a través del *Dockerfile*
* Se modifica el **docker_compose** con unos valores que hacen posible un arranque real del servicio de `vsftp`.

# [1.10.0](/Docker_Files/vsftpd/compare/1.10.0...master)
* Se detecta un problema al usar la imagen detrás de un proxy tipo NGINX, dado que el tráfico es redirigido desde el contenedor se produce un <425 Security: Bad IP connecting>, se corrige el problema dando la posibilidad de habilitar el modo *ftp pasivo promíscuo* con la variable `PASV_PROMISCUOUS`

# [1.9.0](/Docker_Files/vsftpd/compare/1.9.0...master)
* Se actualiza el jenkinsfile segun la nueva versión de la *Shared Library*
* Se retira soporte para armv7

# [1.8.0](/Docker_Files/vsftpd/compare/1.8.0...master)
* Se reemplaza la nomenclatura en el compilador virtual para la arquitectura `linux/arm64/v8`, ahora se denomina `linux/arm64` por **buildx**, reemplazamos valor en variable `platform` del *Jenkinsfile*

# [1.7.1](/Docker_Files/vsftpd/compare/1.7.1...master)
* Se ajusta el `Jenkinsfile` con nueva sintaxis para listados

# [1.7.0](/Docker_Files/vsftpd/compare/1.7.0...master)
* Se ajusta el `Jenkinsfile` con nuevas reglas de validación

# [1.6.0](/Docker_Files/vsftpd/compare/1.6.0...master)
* Se eliminan del `Dockerfile`, las etiquetas genéricas ya que son insertadas al momento de hacer el build de manera automática.
* Se actualiza la configuración por defecto del *.editorconfig* y el *.gitignore*

# [1.5.0](/Docker_Files/vsftpd/compare/1.5.0...master)
* Se simplifican los pipelines usando los estándares segun tipologia, para el caso `dockerfilePipeline`
* Se agrega apunte al `Dockerfile` para el caso que tenga que subirse la imagen a un *Nexus3*.

# [1.4.0](/Docker_Files/vsftpd/compare/1.4.0...master)
* Se corrige problema para pintar logs cuando un contenedor  era reiniciado, solo se pintaba al crear por primera ves ( issue #7 )
* Se optimiza el procedimiento de arranque, se usa el método *wait* para mantener el proceso vivo y no, el por defecto *que es mantenerlo en foreground* ya que de esta manera al cerrar las conexiones SSL, el servicio cae.
* Se actualiza la receta para dar soporte a la arquitectura `linux/arm64/v8`

# [1.3.0](/Docker_Files/vsftpd/compare/1.3.0...master)
* Activado de opción para forzar o no las conexiones por SSL

# [1.2.0](/Docker_Files/vsftpd/compare/1.2.0...master)
* Se corrige problema para enviar logs al stdout del contenedor y además impendía usar el servicio ftp ( issue #3 )
* Se perdonalizar el nombre del servidor ftp
* Se optimiza la ejecución e las configuraciones para hacerlos solo en caso de ser necesarias, como es la creación de carpetas, etc

# [1.1.0](/Docker_Files/vsftpd/compare/1.1.0...master)
* Se documenta la tarea de creación automática de PR en receta Jenkins
* Se cambia el método en el que se redireccionan los logs al contenedor
* Se agrega fichero de licencia de código

# [1.0.0](/Docker_Files/vsftpd/compare/1.0.0...master)
* Inicialización del `CHANGELOG`
* Lintado del ficheros `Vagrantfile`
* Soporte para SSL, usando certificados auto-firmados y comerciales
* Docker compose para pruebas ágiles
* Redirección del log `vsftpd.log` hacia el stdin del contenedor
* Se actualiza la documentación
