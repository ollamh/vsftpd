FROM debian:buster-slim

LABEL DistBase="Debian 10 - Buster"

RUN apt-get update -y && \
	apt-get install -y openssl netcat vsftpd --no-install-recommends && \
	apt-get autoremove -y && apt-get clean -y && \
	rm -rf /var/lib/apt/lists/*

# VSFTPD configuration
COPY config/vsftpd.conf /etc/vsftpd.conf

# VSFTPD pre-configurations
COPY scripts/docker-entrypoint.sh /var/tmp/

RUN chmod +x /var/tmp/docker-entrypoint.sh

ENTRYPOINT ["/var/tmp/docker-entrypoint.sh"]

EXPOSE 20/tcp 21/tcp
EXPOSE 4559/tcp 4560/tcp 4561/tcp 4562/tcp 4563/tcp 4564/tcp

HEALTHCHECK --interval=5m --timeout=3s \
  CMD nc -z localhost 21 || exit 1

CMD ["vsftpd"]
# docker build -t oscarenzo/vsftpd:latest .
# docker buildx build --push -t oscarenzo/vsftpd:latest --platform linux/amd64,linux/arm/v7,linux/arm64/v8 .
# docker buildx build --push -t nexus3.services.arroyof.com:8086/vsftpd:1.x.x --platform linux/amd64,linux/arm/v7 .
